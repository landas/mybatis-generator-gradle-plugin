# 项目结构
- **generator-sample: mybatis-generator-plugin** 的Demo应用，简单地展示插件如何使用  
- **mybatis-generator-plugin: groovy** 封装的 mybatis-generator插件

# build.gradle 配置
- 先编译mybatis-generator-plugin子模块，本地 install 或上传至你的 maven 仓库  
- 然后用如下方法在项目中引入插件（本地使用mavenLocal，或者配置你的 maven url）：  
```
buildscript {
    repositories {
        mavenLocal()
        maven { url "http://maven.aliyun.com/nexus/content/groups/public" }
    }
    dependencies {
        classpath "com.landas:mybatis-generator-plugin:1.0.0"
    }
}

apply plugin: 'java'
apply plugin: 'maven'
apply plugin: 'idea'
apply plugin: 'com.landas.MybatisGenerator'

sourceCompatibility = 1.8

configurations {
    providedRuntime
    mybatisGeneratorConf
}

mybatisGeneratorConf {
    configFile = 'src/main/resources/mybatis-gen-config.xml'
    overwrite = true
    contextIds = "mysql"
    fullyQualifiedTableNames = "user"
    verbose = true
}
```
# 使用
在项目路径下直接运行 runGenerator 任务即可：
```
gradle runGenerator
```

# 配置
## 插件配置
目前gradle插件向外暴露了如下参数：  

|**参数**|**说明**|**示例**|**默认值**|
|:----:|:----|:----|:----|
|**configFile**|xml配置文件的地址|src/main/resources/mybatis-gen-config.xml|mybatis-gen-config.xml|
|**overwrite**|是否覆盖同名文件|true|true|
|**verbose**|是否打印日志到控制台|true|false|
|**targetDir**|生成文件的根目录|.|.|
|**contextIds**|用于过滤configFile文件中的contextId，不填默认为 *。多个ID使用逗号分隔|mysql,postgres|null|
|**fullyQualifiedTableNames**|用于过滤configFile文件中的表名。不填默认使用xml配置的table|foo.bar|null|

## xml 配置
通用配置请参考 generator-sample 模块中的 [mybatis-gen-config.xml](./generator-sample/src/main/resources/mybatis-gen-config.xml)  
支持使用 commentGenerator.addRemarkComments 开启 field 字段注释  
更详细的配置文件参见 mybatis-generator 插件的[原生配置](http://mybatis.org/generator/configreference/classPathEntry.html)  