package com.landas

import org.gradle.api.file.FileCollection
import org.gradle.api.internal.ConventionTask
import org.gradle.api.internal.project.IsolatedAntBuilder
import org.gradle.api.tasks.TaskAction

/**
 * {@code MybatisGeneratorTask} 使用 {@link org.mybatis.generator.ant.GeneratorAntTask} 作为代理实现了 Gradle task

 * @author Maomao Chen
 * @author landas
 * @version 1.0.0
 * @since 1.0.0
 */
class MybatisGeneratorTask extends ConventionTask {
    MybatisGeneratorTask() {
        description = "Mybatis Generator Task which can generate Mapper, XML, etc.."
        group = "Util"
    }

    def configFile
    def overwrite
    def verbose
    def targetDir
    def contextIds
    def fullyQualifiedTableNames
    FileCollection configurationClasspath

    @TaskAction
    void executeCargoAction() {
        services.get(IsolatedAntBuilder).withClasspath(getConfigurationClasspath()).execute {
            ant.taskdef(name: "runGenerator", classname: "org.mybatis.generator.ant.GeneratorAntTask")
            ant.properties['generated.source.dir'] = getTargetDir()
            ant.runGenerator(configFile: getConfigFile(), overwrite: getOverwrite(), verbose: getVerbose(),
                    contextIds: getContextIds(), fullyQualifiedTableNames: getFullyQualifiedTableNames())
        }
    }
}
