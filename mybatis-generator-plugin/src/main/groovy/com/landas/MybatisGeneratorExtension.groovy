package com.landas

import groovy.transform.ToString

/**
 * {@code MybatisGeneratorExtension} 是 mybatis-generator-plugin 对外暴露的配置类。配置属性最后会被写入 {@link com.landas.MybatisGeneratorTask}
 *
 * @author Maomao Chen
 * @author landas
 * @version 1.0.0
 * @since 1.0.0
 * @see org.mybatis.generator.ant.GeneratorAntTask
 */
@ToString(includeNames = true)
class MybatisGeneratorExtension {

    /**
     * The task requires that the attribute "configFile" be set to an existing XML configuration file.
     */
    def configFile = "mybatis-gen-config.xml"

    /**
     * if true, then existing Java files will be overwritten. Otherwise existing Java files will be untouched and generate new files with unique names.
     */
    def overwrite = true

    /**
     * If true, then the generator will log progress messages to the Ant log, Default is false.
     */
    def verbose = false

    /**
     * 生成文件的目标文件夹
     */
    def targetDir = "."

    /**
     * 用于过滤configFile文件中的contextId，不填默认为 *。多个ID使用逗号分隔
     */
    def contextIds

    /**
     * 用于过滤configFile文件中的表名。不填默认使用config文件中的所有table。格式为foo.bar，逗号分隔
     */
    def fullyQualifiedTableNames
}
